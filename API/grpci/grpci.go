package grpci

import (
	"context"
	"fmt"
	"log"
	"scamProtec/blocklist/blocklistpb"
	"scamProtec/invalid/testpb"
	"scamProtec/scam/scampb"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
)

//Call, Event, and Events are all types used to hold json data passed to the API.

type Call struct {
	ANumber string
	BNumber string
}

type Event struct {
	EventId   string       `json:"EventId"`
	Event     scampb.Event `json:"Event"`
	IsScam    bool         `json:"IsScam"`
	BlockCall bool         `json:"BlockCall"`
	Score     int32        `json:"Score"`
}

type Events struct {
	Events []*Event `json:"Event"`
}

//Calls takes post data and uses it to call the blocklist API, which scores a phone call
func Calls(c *gin.Context) {
	//Pull both numbers from the post request
	ANumber := c.PostForm("ANumber")
	BNumber := c.PostForm("BNumber")

	opts := grpc.WithInsecure()

	cc, err := grpc.Dial("invalid:50056", opts)
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	s := testpb.NewTestServiceClient(cc)

	//Create grpc request object
	req := &testpb.IsValidrequest{
		Valid: &testpb.IsValid{
			Number: ANumber,
		},
	}

	//Checks that the calling number is valid.
	res, err := s.Valid(context.Background(), req)
	if err != nil {
		fmt.Print("Error")
		log.Fatalf("error while calling Test RPC: %v", err)
	}

	fmt.Println(res)

	//If the calling number is not valid, return an error.
	if !res.Result {
		c.JSON(400, gin.H{
			"Valid": "The ANumber is not valid",
		})
		return
	}

	fmt.Println("Blog Client")

	bb, err := grpc.Dial("blocklist:50052", opts)
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close() // Maybe this should be in a separate function and the error handled?

	b := blocklistpb.NewBlocklistServiceClient(bb)

	//Create grpc request to score phone call
	blockreq := &blocklistpb.ScoreEventRequest{
		EventId:   strconv.Itoa(int(time.Now().UnixNano())),
		ANumber:   ANumber,
		BNumber:   BNumber,
		CallTime:  time.Now().UnixNano(),
		SipInvite: "",
	}
	//Score the phone call
	blockres, err := b.ScoreEvent(context.Background(), blockreq)

	if err != nil {

		log.Println(err)

	}

	if err != nil {
		log.Println(err)
	}
	fmt.Println(blockres)

	//Return the results of the scoring
	c.JSON(200, gin.H{
		"eventid":   blockres.GetEventId(),
		"isScam":    blockres.GetIsScam(),
		"blockCall": blockres.GetBlockCall(),
		"metadata":  blockres.GetMetadata(),
	})

}

//Batch takes a list of call events in JSON format and scores them. It returns a json object with the scoring results.
func Batch(c *gin.Context) {
	//Pull call events from JSON
	var calls []Call
	c.BindJSON(&calls)
	opts := grpc.WithInsecure()
	cc, err := grpc.Dial("invalid:50056", opts)
	bb, err := grpc.Dial("blocklist:50052", opts)

	//Loop through each call in the request and score them
	for i, call := range calls {

		if err != nil {
			log.Fatalf("could not connect: %v", err)
		}

		s := testpb.NewTestServiceClient(cc)

		//Define grpc request to for number validation.
		req := &testpb.IsValidrequest{
			Valid: &testpb.IsValid{
				Number: call.ANumber,
			},
		}

		//Check that calling number is valid
		res, err := s.Valid(context.Background(), req)
		if err != nil {
			fmt.Print("Error")
			log.Fatalf("error while calling Test RPC: %v", err)
		}

		fmt.Println(res)

		//If calling number is not valid, return an error.
		if !res.Result {
			c.JSON(200, gin.H{
				"eventid": strconv.FormatInt(int64(i), 10),
				"Valid":   "The ANumber is not valid",
			})
		} else {

			fmt.Println("Blog Client")

			if err != nil {
				log.Fatalf("could not connect: %v", err)
			}

			b := blocklistpb.NewBlocklistServiceClient(bb)

			//Define request for scoring a phone call event
			blockreq := &blocklistpb.ScoreEventRequest{
				EventId:   strconv.Itoa(int(time.Now().UnixNano())),
				ANumber:   call.ANumber,
				BNumber:   call.BNumber,
				CallTime:  time.Now().UnixNano(),
				SipInvite: "",
			}
			//Score the event
			blockres, err := b.ScoreEvent(context.Background(), blockreq)

			if err != nil {

				log.Println(err)

			}

			if err != nil {
				log.Println(err)
			}
			fmt.Println(blockres)

			//Return the scoring results
			c.JSON(200, gin.H{
				"eventid":   blockres.GetEventId(),
				"isScam":    blockres.GetIsScam(),
				"blockCall": blockres.GetBlockCall(),
				"metadata":  blockres.GetMetadata(),
			})

		}
	}
	defer bb.Close()
	defer cc.Close()

}

//Results returns all the scoring events in MongoDB as JSON
func Results(c *gin.Context) {
	opts := grpc.WithInsecure()
	cc, err := grpc.Dial("scam:50050", opts)

	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}

	defer cc.Close()

	client := scampb.NewScamDatabaseClient(cc)

	//Call gRPC service to get scoring results
	response, err := client.ListScoringResults(c, &scampb.ListScoringResultsRequest{})
	if err != nil {
		log.Fatalf("error listing scoring results %v\n", err)
	}

	//Put scoring results in JSON format and return
	var events []*Event
	for _, result := range response.GetScoringResults() {
		events = append(events, &Event{
			EventId:   result.GetEventId(),
			Event:     *result.GetEvent(),
			IsScam:    result.GetIsScam(),
			BlockCall: result.GetBlockCall(),
			Score:     result.GetScore(),
		})
	}

	c.JSON(200, events)
}

//PhoneNumber returns the category_code, category_name, blocked numbers, and blocked categories for the requested phone number
func PhoneNumber(c *gin.Context) {
	//Gets the phoneNumber from the get request
	Number := c.Param("phoneNumber")

	opts := grpc.WithInsecure()
	cc, err := grpc.Dial("scam:50050", opts)

	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	client := scampb.NewScamDatabaseClient(cc)

	//Calls the gRPC to get the phone number blocklists
	blockres, err := client.GetPersonalBlocklist(context.Background(), &scampb.GetPersonalBlocklistRequest{PhoneNumber: Number})
	if err != nil {
		fmt.Printf("error fetching blocklist: %v\n", err)

		c.JSON(404, gin.H{
			"Error": err,
		})
		return
	}

	//Calls the grpc to get the phone number category and code
	catres, err := client.GetPhoneCategory(context.Background(), &scampb.GetPhoneCategoryRequest{PhoneNumber: Number})
	if err != nil {
		fmt.Printf("error fetching category: %v\n", err)

		c.JSON(404, gin.H{
			"Error": err,
		})
		return
	}

	//Returns JSON object with phone number information.
	c.JSON(200, gin.H{
		"category_code":      catres.GetCategoryCode(),
		"category_name":      catres.GetCategoryName(),
		"blocked_a_numbers":  blockres.GetBlockedANumbers(),
		"blocked_categories": blockres.GetBlockedCategories(),
	})
}
