package main

import (
	"scamProtec/API/grpci"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {

	app := gin.Default()

	//Configure CORS header to allow requests from all origins
	app.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	//Creating endpoints
	app.POST("/api/call-protection/batch", grpci.Batch)

	app.POST("/api/call-protection/calls", grpci.Calls)

	app.GET("/api/call-protection/results", grpci.Results)

	app.GET("/api/call-protection/phone/:phoneNumber", grpci.PhoneNumber)

	app.Run("0.0.0.0:50054")
}
