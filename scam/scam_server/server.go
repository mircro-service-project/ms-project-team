package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"scamProtec/scam/scampb"

	"gopkg.in/mgo.v2/bson"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"google.golang.org/grpc"
)

var phone_collection *mongo.Collection
var score_collection *mongo.Collection

type server struct {
	scampb.ScamDatabaseServer
}

// PhoneNumbers defines entry for PhoneNumbers collection in MongoDB
type PhoneNumbers struct {
	ID                primitive.ObjectID `bson:"_id,omitempty"`
	Number            string             `bson:"_number,omitempty"`
	CategoryCode      string             `bson:"_category_code,omitempty"`
	CategoryName      string             `bson:"_category_name,omitempty"`
	BlockedANumbers   []string           `bson:"_blocked_numbers,omitempty"`
	BlockedCategories []string           `bson:"_blocked_categories,omitempty"`
	Score             int32              `bson:"_score, omitempty"`
}

// Score defines entry for Score collection in MongoDB
type Score struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	EventId   string             `bson:"_event_id, omitempty"`
	Event     *scampb.Event      `bson:"_event, omitempty"`
	BlockCall bool               `bson:"_block_call, omitempty"`
	IsScam    bool               `bson:"_is_scam, omitempty"`
	Score     int32              `bson:"_score, omitempty"`
}

//PutScoringResult adds the results of a scoring event to the Score collection in MongoDB
func (*server) PutScoringResult(ctx context.Context, req *scampb.PutScoringResultRequest) (*scampb.PutScoringResultResponse, error) {
	fmt.Println("Add score request")

	data := Score{
		EventId:   req.GetScoringResult().GetEventId(),
		Event:     req.GetScoringResult().GetEvent(),
		BlockCall: req.GetScoringResult().GetBlockCall(),
		IsScam:    req.GetScoringResult().GetIsScam(),
		Score:     req.GetScoringResult().GetScore(),
	}

	_, err := score_collection.InsertOne(ctx, data)
	if err != nil {
		return &scampb.PutScoringResultResponse{},
			status.Errorf(
				codes.Internal,
				fmt.Sprintf("Internal error: %v", err),
			)
	}
	return &scampb.PutScoringResultResponse{}, nil
}

//AddPhoneNumber adds a phone number to the PhoneNumber collection in MongoDB
func (*server) AddPhoneNumber(ctx context.Context, req *scampb.AddPhoneNumberRequest) (*scampb.AddPhoneNumberResponse, error) {
	fmt.Println("Add number request")

	data := PhoneNumbers{
		Number:            req.GetNumber(),
		CategoryCode:      req.GetCategory_Code(),
		CategoryName:      req.GetCategory_Name(),
		BlockedANumbers:   req.GetBlockedANumbers(),
		BlockedCategories: req.GetBlockedCategories(),
		Score:             req.GetScore(),
	}

	res, err := phone_collection.InsertOne(ctx, data)
	if err != nil {
		return &scampb.AddPhoneNumberResponse{
				Success: false,
			}, status.Errorf(
				codes.Internal,
				fmt.Sprintf("Internal error: %v", err),
			)
	}
	_, ok := res.InsertedID.(primitive.ObjectID)
	if !ok {
		return &scampb.AddPhoneNumberResponse{
				Success: false,
			}, status.Errorf(
				codes.Internal,
				fmt.Sprintf("Cannot convert to OID"),
			)
	}

	return &scampb.AddPhoneNumberResponse{
		Success: true,
	}, nil

}

//ListScoringResults returns a list of all entries in the Score collection in MongoDB
func (*server) ListScoringResults(ctx context.Context, req *scampb.ListScoringResultsRequest) (*scampb.ListScoringResultsResponse, error) {
	cursor, err := score_collection.Find(ctx, bson.M{})

	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(ctx)

	var scores []*scampb.ScoringResult

	//Loop through all entries in Score collection and add them to result array
	for cursor.Next(ctx) {
		var score Score
		err := cursor.Decode(&score)
		if err != nil {
			log.Fatalf("cursor.next() %v\n", err)
		}

		scores = append(scores, &scampb.ScoringResult{
			EventId:   score.EventId,
			Event:     score.Event,
			BlockCall: score.BlockCall,
			IsScam:    score.IsScam,
			Score:     score.Score,
		})
	}
	return &scampb.ListScoringResultsResponse{ScoringResults: scores}, nil
}

//GetPhoneCategory returns the category name and category code of the requested phone number
func (*server) GetPhoneCategory(ctx context.Context, req *scampb.GetPhoneCategoryRequest) (*scampb.GetPhoneCategoryResponse, error) {
	fmt.Println("Read Phone Category request")

	PhoneNumber := req.GetPhoneNumber()

	// create an empty struct
	data := &PhoneNumbers{}
	filter := bson.M{"_number": PhoneNumber}

	res := phone_collection.FindOne(ctx, filter)
	if err := res.Decode(data); err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("Cannot find number with specified ID: %v", err),
		)
	}

	return &scampb.GetPhoneCategoryResponse{
		CategoryName: data.CategoryName,
		CategoryCode: data.CategoryCode,
	}, nil

}

//GetScore returns the score associated with the requested phone number
func (*server) GetScore(ctx context.Context, req *scampb.GetScoreRequest) (*scampb.GetScoreResponse, error) {
	fmt.Println("Read blog request")

	PhoneNumber := req.GetPhoneNumber()

	// create an empty struct
	data := &PhoneNumbers{}
	filter := bson.M{"_number": PhoneNumber}

	res := phone_collection.FindOne(ctx, filter)
	if err := res.Decode(data); err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("Cannot find number with specified ID: %v", err),
		)
	}

	return &scampb.GetScoreResponse{
		Score: data.Score,
	}, nil

}

//GetPersonalBlocklist returns the blocked numbers and blocked categories associated with the requested phone number
func (*server) GetPersonalBlocklist(ctx context.Context, req *scampb.GetPersonalBlocklistRequest) (*scampb.GetPersonalBlocklistResponse, error) {
	fmt.Println("Read blog request")

	PhoneNumber := req.GetPhoneNumber()

	// create an empty struct
	data := &PhoneNumbers{}
	filter := bson.M{"_number": PhoneNumber}

	res := phone_collection.FindOne(ctx, filter)
	if err := res.Decode(data); err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("Cannot find number with specified ID: %v", err),
		)
	}

	return &scampb.GetPersonalBlocklistResponse{
		BlockedANumbers:   data.BlockedANumbers,
		BlockedCategories: data.BlockedCategories,
	}, nil
}

func main() {
	// if we crash the go code, we get the file name and line number
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	fmt.Println("Connecting to MongoDB")
	// connect to MongoDB
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://mongo:27017"))
	if err != nil {
		log.Fatal(err)
	}
	err = client.Connect(context.TODO())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Scam Service Started")
	phone_collection = client.Database("mydb").Collection("PhoneNumbers")
	score_collection = client.Database("mydb").Collection("Scores")

	//Restrict the _number field to contain unique values only. NO DUPLICATE PHONE NUMBERS.
	_, err = phone_collection.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    bson.M{`_number`: 1},
			Options: options.Index().SetUnique(true),
		},
	)
	if err != nil {
		log.Printf("error on set uniqe %v\n", err)
	}

	//Restrict the _event_id field to contain unique values only. NO DUPLICATE EVENT IDS.
	_, err = phone_collection.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    bson.M{`_event_id`: 1},
			Options: options.Index().SetUnique(true),
		},
	)
	if err != nil {
		log.Printf("error on set uniqe %v\n", err)
	}

	lis, err := net.Listen("tcp", "0.0.0.0:50050")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	var opts []grpc.ServerOption
	s := grpc.NewServer(opts...)
	scampb.RegisterScamDatabaseServer(s, &server{})
	// Register reflection service on gRPC server.
	reflection.Register(s)

	go func() {
		fmt.Println("Starting Server...")
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	// Wait for Control C to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal is received
	<-ch
	// First we close the connection with MongoDB:
	fmt.Println("Closing MongoDB Connection")
	if err := client.Disconnect(context.TODO()); err != nil {
		log.Fatalf("Error on disconnection with MongoDB : %v", err)
	}

	// Finally, we stop the server
	fmt.Println("Stopping the server")
	s.Stop()
	fmt.Println("End of Program")
}
