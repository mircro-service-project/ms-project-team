package main

import (
	"context"
	"fmt"
	"log"
	"scamProtec/scam/scampb"

	"google.golang.org/grpc"
)

func main() {

	fmt.Println("Blog Client")

	opts := grpc.WithInsecure()

	cc, err := grpc.Dial("localhost:50050", opts)
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close() // Maybe this should be in a separate function and the error handled?

	c := scampb.NewScamDatabaseClient(cc)

	fmt.Println("adding scores")
	Scores := &scampb.PutScoringResultRequest{
		ScoringResult: &scampb.ScoringResult{
			EventId: "002",
			Event: &scampb.Event{
				ANumber:  "whocares2",
				BNumber:  "yup",
				CallTime: 5,
			},
			BlockCall: true,
			IsScam:    false,
			Score:     5,
		},
	}
	_, err = c.PutScoringResult(context.Background(), Scores)
	if err != nil {
		log.Fatalf("Put Score err: %v\n", err)
	}

	res, err := c.ListScoringResults(context.Background(), &scampb.ListScoringResultsRequest{})
	if err != nil {
		log.Fatalf("Response err: %v\n", err)
	}
	responses := res.GetScoringResults()

	for _, response := range responses {
		fmt.Println(response)
	}

	// create Blog
	fmt.Println("Adding Numbers")
	PhoneNumbers := &scampb.AddPhoneNumberRequest{
		Number:            "5015555555",
		Category_Code:     "AB",
		Category_Name:     "Alphabetl",
		BlockedANumbers:   append(make([]string, 1), "4798437654"),
		BlockedCategories: append(make([]string, 1), "ps"),
		Score:             54,
	}
	addNumberResponse, err := c.AddPhoneNumber(context.Background(), PhoneNumbers)
	if err != nil {
		log.Fatalf("Unexpected error: %v", err)
	}
	fmt.Printf("Blog has been created: %v", addNumberResponse)
	//blogID := createBlogRes.GetBlog().GetId()
	//
	//// read Blog
	//fmt.Println("Reading the blog")
	//
	//_, err2 := c.ReadBlog(context.Background(), &blogpb.ReadBlogRequest{BlogId: "5bdc29e661b75adcac496cf4"})
	//if err2 != nil {
	//	fmt.Printf("Error happened while reading: %v \n", err2)
	//}
	//
	//readBlogReq := &blogpb.ReadBlogRequest{BlogId: blogID}
	//readBlogRes, readBlogErr := c.ReadBlog(context.Background(), readBlogReq)
	//if readBlogErr != nil {
	//	fmt.Printf("Error happened while reading: %v \n", readBlogErr)
	//}
	//
	//fmt.Printf("Blog was read: %v \n", readBlogRes)
	//
	//// update Blog
	//newBlog := &blogpb.Blog{
	//	Id:       blogID,
	//	AuthorId: "Changed Author",
	//	Title:    "My First Blog (edited)",
	//	Content:  "Content of the first blog, with some awesome additions!",
	//}
	//updateRes, updateErr := c.UpdateBlog(context.Background(), &blogpb.UpdateBlogRequest{Blog: newBlog})
	//if updateErr != nil {
	//	fmt.Printf("Error happened while updating: %v \n", updateErr)
	//}
	//fmt.Printf("Blog was updated: %v\n", updateRes)
	//
	//// delete Blog
	//deleteRes, deleteErr := c.DeleteBlog(context.Background(), &blogpb.DeleteBlogRequest{BlogId: blogID})
	//
	//if deleteErr != nil {
	//	fmt.Printf("Error happened while deleting: %v \n", deleteErr)
	//}
	//fmt.Printf("Blog was deleted: %v \n", deleteRes)
	//
	//// list Blogs
	//
	//stream, err := c.ListBlog(context.Background(), &blogpb.ListBlogRequest{})
	//if err != nil {
	//	log.Fatalf("error while calling ListBlog RPC: %v", err)
	//}
	//for {
	//	res, err := stream.Recv()
	//	if err == io.EOF {
	//		break
	//	}
	//	if err != nil {
	//		log.Fatalf("Something happened: %v", err)
	//	}
	//	fmt.Println(res.GetBlog())
	//}
}
