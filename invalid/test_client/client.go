package main

import (
	"context"
	"fmt"
	"log"
	"scamProtec/invalid/testpb"

	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Hello I'm a client")

	opts := grpc.WithInsecure()

	cc, err := grpc.Dial("localhost:50053", opts)
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	c := testpb.NewTestServiceClient(cc)
	// fmt.Printf("Created client: %f", c)

	doUnary(c)
}

func doUnary(c testpb.TestServiceClient) {
	fmt.Println("Starting to do a Unary RPC...")
	req := &testpb.IsValidrequest{
		Valid: &testpb.IsValid{
			Number: "5013398428",
		},
	}
	res, err := c.Valid(context.Background(), req)
	if err != nil {
		fmt.Print("Error")
		log.Fatalf("error while calling Test RPC: %v", err)
	}
	log.Printf("Phone Number Valid testing (%s): %v", req.Valid.Number, res.Result)
}
