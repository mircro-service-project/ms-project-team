package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"scamProtec/invalid/testpb"

	"github.com/nyaruka/phonenumbers"
	"google.golang.org/grpc"
)

//Server struct
type server struct {
	testpb.UnimplementedTestServiceServer
}

//Funtion Valid
//Params: context, request
//Returns: Response, Error
func (*server) Valid(ctx context.Context, req *testpb.IsValidrequest) (*testpb.IsValidresponse, error) {
	//logging
	fmt.Printf("IsValid function was invoked with %v\n", req)
	Number := req.GetValid().GetNumber()
	//Takes number and turns it into usable US number
	num, err := phonenumbers.Parse(Number, "US")
	if err != nil {
		//logs error
		log.Fatal(err)
	}
	//creates response
	res := &testpb.IsValidresponse{
		Result: phonenumbers.IsValidNumber(num),
	}
	return res, nil
}

func main() {
	//logging
	fmt.Println("Hello world")
	//listens on port 50056
	lis, err := net.Listen("tcp", "0.0.0.0:50056")
	if err != nil {
		//logs error and fatals
		log.Fatalf("Failed to listen: %v", err)
	}

	opts := []grpc.ServerOption{}

	s := grpc.NewServer(opts...)
	testpb.RegisterTestServiceServer(s, &server{})
	//accept incoming connnections on the listener port
	if err := s.Serve(lis); err != nil {
		//logs error and fatals
		log.Fatalf("failed to serve: %v", err)
	}
}
