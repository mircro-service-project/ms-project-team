package main

import (
	"context"
	"fmt"
	"log"
	"scamProtec/blocklist/blocklistpb"
	"time"

	"google.golang.org/grpc"
)

func main() {

	fmt.Println("Blog Client")

	opts := grpc.WithInsecure()

	cc, err := grpc.Dial("localhost:50052", opts)
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close() // Maybe this should be in a separate function and the error handled?

	c := blocklistpb.NewBlocklistServiceClient(cc)

	req := &blocklistpb.ScoreEventRequest{
		EventId:   "001",
		ANumber:   "5016556093",
		BNumber:   "4172483987",
		CallTime:  time.Now().UnixNano(),
		SipInvite: "whocares",
	}
	stream, err := c.ScoreEvents(context.Background())

	if err != nil {
		log.Println(err)
	}
	stream.Send(req)
	time.Sleep(1000 * time.Millisecond)
	fmt.Println(stream.Recv())
	stream.Send(req)
	time.Sleep(1000 * time.Millisecond)
	fmt.Println(stream.Recv())
	stream.Send(req)
	time.Sleep(1000 * time.Millisecond)
	fmt.Println(stream.Recv())
	err = stream.CloseSend()
	if err != nil {
		log.Fatal(err)
	}

}
